# Onion MkDocs: Material template for Tor documentation

This is a [MkDocs][] documentation template for [Tor][]-related projects and
based on the [Material theme][].

[Tor]: https://torproject.org
[MkDocs]: https://www.mkdocs.org/
[Material theme]: https://squidfunk.github.io/mkdocs-material/

## HOWTO

This HOWTO assumes an Onion MkDocs setup using a [CI template][], which will
work if you're hosting your documentation at [gitlab.torproject.org][]. Other
installation methods are detailed below.

First, add the following at your [.gitlab-ci.yml][] file:

```yaml
include:
  - project: tpo/web/onion-mkdocs
    file: .gitlab-ci-base.yml

pages:
  extends: .onion-mkdocs-clone
```

Then create a [mkdocs.yml][] configuration. A minimal config could be:

```yaml
INHERIT: vendors/onion-mkdocs/onion-mkdocs.yml

site_name: My Awesome Documentation
repo_url : https://gitlab.torproject.org/my-user/my-awesome-docs
site_url : https://my-user.pages.torproject.net/my-awesome-docs
```

Now it's time to put your docs in place. Create a `docs/README.md` file
with some [Markdown][] content on it.

The next step is to make sure your repository have CI enabled (usually under
"Settings -> General -> Visibility, project features, permissions").

Finally, commit these new files and push to your remote repository.

It's as easy as that! Once these changes are pushed, [GitLab CI][] will kick in
and build your Onion MkDocs website.

[CI template]: https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/.gitlab-ci-base.yml
[gitlab.torproject.org]: https://gitlab.torproject.org
[.gitlab-ci.yml]: https://docs.gitlab.com/ee/ci/yaml/
[mkdocs.yml]: https://www.mkdocs.org/user-guide/configuration/
[Markdown]: https://www.mkdocs.org/user-guide/writing-your-docs/#writing-with-markdown
[GitLab CI]: https://docs.gitlab.com/ee/ci/

## Repository

Check the repository at
[https://gitlab.torproject.org/tpo/web/onion-mkdocs](https://gitlab.torproject.org/tpo/web/onion-mkdocs).

## Requirements

Onion MkDocs relies the following software:

* [MkDocs][] as the web frameworks.
* [Pipenv](https://pipenv.pypa.io/en/latest/),
  [pip](https://pypi.org/project/pip/) or other package manager to handle
  Python dependencies.
* The [Material theme][] for all the styling.

Optional dependencies:

* [GNU Make](https://www.gnu.org/software/make) for building.

Example installation procedures tested on Debian Bullseye are available:

* [scripts/onion-mkdocs-provision-build](https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/scripts/onion-mkdocs-provision-build):
  installs all the needed dependencies to build Onion MkDocs websites.
* [scripts/onion-mkdocs-provision-host](https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/scripts/onion-mkdocs-provision-host):
  provision the hosting environment to serve Onion MkDocs websites, including
  a [Tor Onion Service](https://community.torproject.org/onion-services/).

## Repository setup

This repository is ready to be used in any existing repository.
It can be installed in a number of ways as follows.

### Forking the project

Simply fork this project and change whatever you need.

### Manually copying

Even simpler, copy all the relevant files from this repository into your project.

### Vendorizing

You can also vendorize Onion MkDocs by reserving a folder for it like in a
`vendor` (or `vendors`) inside your project.

#### Vendorization strategies

There are several ways to vendor it:

0. Use Onion MkDocs directly from it's remote repository (with GitLab CI
   templating functionality).

1. As a Git submodule: simply put it as a Git submodule somewhere:

        mkdir -p vendors && \
        git submodule add --depth 1 https://gitlab.torproject.org/tpo/web/onion-mkdocs vendors/onion-mkdocs

2. Regular cloning:

        mkdir -p vendors && \
        git clone --depth 1 https://gitlab.torproject.org/tpo/web/onion-mkdocs vendors/onion-mkdocs && \
        git ignore vendors/onion-mkdocs

3. Using `git-subtree(1)`.

#### Configuration

After vendorization, make sure to create the needed configuration files.

You can either copy some of these files from the Onion MkDocs repository or use
symbolic links to reference all the needed files from the root of your
repository.

The recommended files can be set as follows:

```shell
    mkdir docs
    (
      cd docs
      ln -s ../vendors/onion-mkdocs/docs/site.css
      cd assets
      ln -s ../../vendors/onion-mkdocs/docs/assets/tor-logo1x.png
      ln -s ../../vendors/onion-mkdocs/docs/assets/favicon.png
    )

    cp vendors/onion-mkdocs/mkdocs.yml . || touch mkdocs.yml # then customize
```

##### CI

If you plan to use either GitLab CI/CD or GitHub Actions to build your
documentation, make sure to at least copy/adapt the corresponding files from
the submodule. You can't just symlink those since the submodule won't be
accessible when GitLab and GitHub looks for CI/CD or Actions files. The
provided GitLab CI/CD and GitHub actions configuration takes care of
initializing the submodule during build time, but they should be available in
the main repo in order to do so.

The following `gitlab-ci.yml` configuration uses a CI template and does not
require you to manually vendorize Onion MkDocs in a subfolder (but requires
that Onion MkDocs is reachable at `tpo/web/onion-mkdocs` in the GitLab
instance):

```yaml
include:
  # Customize the project location depending on the GitLab instance in use
  project: tpo/web/onion-mkdocs
  file: .gitlab-ci-base.yml

pages:
  extends: .onion-mkdocs-clone
```

## Compiling

### Locally

Locally compilation is supported if the Onion MkDocs code is available somewhere in your
project tree.

Once all dependencies are installed and the required files are in place in your repository, just
type the following to build your Onion MkDocs website:

    vendor/onion-mkdocs/scripts/onion-mkdocs-build

Or, if you have setup the proper Makefile targets:

    make compile

### Using CI

Compilation using CI (GitLab or GitHub) will happen automatically as long as
the proper configuration is in place.

## Serve

As an alias to `mkdocs(1)` `serve` command, type

    vendor/onion-mkdocs/scripts/onion-mkdocs-serve

Or, if you have setup the proper Makefile targets:

    make serve

## Watching

A basic "watch" mode to rebuild your site whenever the docs change (like a live edit mode), use

    vendor/onion-mkdocs/scripts/onion-mkdocs-watch

Or, if you have setup the proper Makefile targets:

    make watch

## Formatting support

### MkDocs extensions

Supports all [default MkDocs extensions](https://www.mkdocs.org/user-guide/configuration/#markdown_extensions).

### Tasklists

Supports [Material theme][]'s [tasklist](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/?h=#tasklist),
which is compatible with [GitLab Flavored Markdown (GLFM)](https://docs.gitlab.com/ee/user/markdown.html).

### Diagrams

Onion MkDocs also comes with [Diagrams](https://squidfunk.github.io/mkdocs-material/reference/diagrams/) support:

```mermaid
graph LR
  Write --> Build --> Share
```

### Other formatting plugins

All [other markdown extensions][] from the [Material theme][] are also
available by additional configuration.

[other markdown extensions]: https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/

## GitLab pages support

Onion MkDocs supports GitLab pages out-of-the box with a [simple GitLab CI/CD
configuration][].

[simple GitLab CI/CD configuration]: https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/.gitlab-ci-pages.yml

## GitHub pages support

Onion MkDocs also supports GitHub pages out-of-the box with a [simple GitHub CI/CD
configuration][].

[simple GitHub CI/CD configuration]: https://gitlab.torproject.org/tpo/web/onion-mkdocs/-/blob/main/.github/gh-pages.yml

## Onionshare support

It's possible to share your local Onion MkDocs build with others by using [OnionShare][].
Ensure you have a working [onionshare-cli installation][], the proper Makefile targets
and then type:

    make share

Can even be combined with `make watch` to keep the shared site always up-to-date (untested).

[OnionShare]: https://onionshare.org
[onionshare-cli installation]: https://docs.onionshare.org/2.6/en/advanced.html#command-line-interface

## Live examples

Check out some live examples of Onion MkDocs config in action:

* [The Onion Plan](https://tpo.pages.torproject.net/onion-services/onionplan/).
* [The Oniongroove](https://tpo.pages.torproject.net/onion-services/oniongroove/).
* [Onionprobe](https://tpo.pages.torproject.net/onion-services/onionprobe/).
